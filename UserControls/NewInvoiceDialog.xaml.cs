﻿using Kontrahenci.Model;
using Kontrahenci.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kontrahenci.UserControls
{
    /// <summary>
    /// Interaction logic for NewInvoiceDialog.xaml
    /// </summary>
    public partial class NewInvoiceDialog : UserControl
    {
        public NewInvoiceDialog(object contractor, bool editMode = false)
        {
            InitializeComponent();
            Contractor _contractor = contractor as Contractor;
            NewInvoiceDialogVM vm = new NewInvoiceDialogVM(_contractor,editMode);
            DataContext = vm;
        }

    }
}
