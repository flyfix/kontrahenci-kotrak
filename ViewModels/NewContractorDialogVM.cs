﻿using Kontrahenci.Common;
using Kontrahenci.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kontrahenci.ViewModels
{
    public class NewContractorDialogVM : ViewModelBase
    {
        bool editMode;

        public NewContractorDialogVM(object _contractor = null,bool _editMode = false) 
        {   
            editMode = _editMode;
            Contractor contractor = _contractor as Contractor;
            if (_editMode && _contractor != null)
            {
                NewContractor = GetContractorById(contractor.Id);
            }
            else
            {
                NewContractor = new Contractor();
            }
            
        }
      
        #region Commands && Functions
        private Contractor GetContractorById(Int32 id)
        {
            try
            {
                Contractor contractor = (from c in dbContext.ContractorSet
                                         where c.Id == id
                                         select c).FirstOrDefault();
                return contractor;
            }
            catch (Exception ex)
            {

                dbContext.Dispose();
                throw new NotImplementedException(ex.Message, ex); //TODO

            }
        }

        private ICommand _addContractorCommand;
        public ICommand AddNewContractorCommand
        {
            get
            {
                if (_addContractorCommand == null)
                {
                    _addContractorCommand = new DelegateCommand();
                    ((DelegateCommand)_addContractorCommand).InitializeCommand(val => AddNewContractor(), cal => CanAddNewContractor());
                }
                return _addContractorCommand;
            }
        }

        private bool CanAddNewContractor()
        {
            if(NewContractor.Name != null && NewContractor.Name != String.Empty &&
                NewContractor.City != null && NewContractor.City != String.Empty &&
                NewContractor.Email != null && NewContractor.Email != String.Empty)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void AddNewContractor()
        {
            try
            {
                dbContext.ContractorSet.Add(NewContractor);
            }
            catch (Exception ex)
            {

                dbContext.Dispose();
                throw new NotImplementedException(ex.Message, ex); //TODO

            }
            ClearFields();

        }
        private void ClearFields()
        {
            NewContractor = new Contractor();
        }

        private ICommand _saveChangesCommand;
        public ICommand SaveChangesCommand
        {
            get
            {
                if (_saveChangesCommand == null)
                {
                    _saveChangesCommand = new DelegateCommand();
                    ((DelegateCommand)_saveChangesCommand).InitializeCommand(val => SaveChangesInDB());
                }
                return _saveChangesCommand;
            }
        }
        new private  void SaveChangesInDB()
        {
            
            base.SaveChangesInDB();
            ClearFields();
        }

        #endregion
        #region get/set
        private Contractor _newContractor;
        public Contractor NewContractor
        {
            get
            {
                return _newContractor;
            }
            set
            {
                if (_newContractor != value)
                {
                    _newContractor = value;
                    NotifyPropertyChanged(() => NewContractor);
                }
            }
        }

      

       
        #endregion


    }
}
