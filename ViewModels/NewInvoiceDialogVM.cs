﻿using Kontrahenci.Common;
using Kontrahenci.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kontrahenci.ViewModels
{
    class NewInvoiceDialogVM : ViewModelBase
    {
        public NewInvoiceDialogVM() { }
        public NewInvoiceDialogVM(object contractor, object invoice = null, bool _editMode = false)
        {
            CurrentConractor = contractor as Contractor;
            NewInvoice = new Invoice();
            editMode = _editMode;
        }
        private bool editMode;
        #region get/set
        private Invoice _newInvoice;
        public Invoice NewInvoice
        {
            get
            {
                return _newInvoice;
            }
            set
            {
                if (_newInvoice != value)
                {
                    _newInvoice = value;
                    NotifyPropertyChanged(() => NewInvoice);
                }
            }
        }

        #endregion

        #region Commands && Functions
        private Contractor _currentConractor;
        public Contractor CurrentConractor
        {
            get
            {
                return _currentConractor;
            }
            set
            {
                if (_currentConractor != value)
                {
                    _currentConractor = value;
                    NotifyPropertyChanged(() => CurrentConractor);
                }
            }
        }

        private ICommand _saveChangesCommand;
        public ICommand SaveChangesCommand
        {
            get
            {
                if (_saveChangesCommand == null)
                {
                    _saveChangesCommand = new DelegateCommand();
                    ((DelegateCommand)_saveChangesCommand).InitializeCommand(val => SaveChangesInDB());
                }
                return _saveChangesCommand;
            }
        }
        new private void SaveChangesInDB()
        {
            
            base.SaveChangesInDB();
            ClearFields();
        }

        private void ClearFields()
        {
            NewInvoice = new Invoice();
        }

        private ICommand _addNewInvoiceCommand;
        public ICommand AddNewInvoiceCommand
        {
            get
            {
                if (_addNewInvoiceCommand == null)
                {
                    _addNewInvoiceCommand = new DelegateCommand();
                    ((DelegateCommand)_addNewInvoiceCommand).InitializeCommand(val => AddNewInvoice(), cal => CanAddNewInvoice());
                }
                return _addNewInvoiceCommand;
            }
        }

        private bool CanAddNewInvoice()
        {

            if (NewInvoice.Name != null && NewInvoice.Name != String.Empty &&
                NewInvoice.Amount != 0 && NewInvoice.Description != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void AddNewInvoice()
        {
            try
            {
                var contractor = (from c in dbContext.ContractorSet
                                    where c.Id == CurrentConractor.Id
                                    select c).FirstOrDefault();
                contractor.Invoice.Add(NewInvoice);
                ClearFields();
            }

            catch (Exception ex)
            {

                dbContext.Dispose();
                throw new NotImplementedException(ex.Message, ex); //TODO

            }

        }
        #endregion
    }
}
