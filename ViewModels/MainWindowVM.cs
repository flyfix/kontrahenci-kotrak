﻿using Kontrahenci.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Kontrahenci.Common;
using System.Windows.Input;
using System.Windows;
using Kontrahenci.UserControls;

namespace Kontrahenci.ViewModels
{
    public class MainWindowVM : ViewModelBase
    {
        public MainWindowVM()
        {
            RefreshContractors();
        }

        
        #region get/set
        private ObservableCollection<Contractor> _contractorCollection;
        public ObservableCollection<Contractor> ContractorCollection
        {
            get
            {
                return _contractorCollection;
            }
            set
            {
                if (_contractorCollection != value)
                {
                    _contractorCollection = value;
                    NotifyPropertyChanged(() => ContractorCollection);
                }
            }
        }

        private ObservableCollection<Invoice> _contractorInvoiceCollection;
        public ObservableCollection<Invoice> ContractorInvoiceCollection
        {
            get
            {
                return _contractorInvoiceCollection;
            }
            set
            {
                if (_contractorInvoiceCollection != value)
                {
                    _contractorInvoiceCollection = value;
                    NotifyPropertyChanged(() => ContractorInvoiceCollection);
                }
            }
        }



        private Contractor _selectedContractor;
        public Contractor SelectedContractor
        {
            get
            {
                return _selectedContractor;
            }
            set
            {
                if (_selectedContractor != value)
                {
                    _selectedContractor = value;
                    NotifyPropertyChanged(() => SelectedContractor);
                    RefreshInvoices();
                }
            }
        }
        #endregion

        #region Commands && Functions

        private void RefreshContractors()
        {
            try
            {
                ContractorCollection = new ObservableCollection<Contractor>((from c in dbContext.ContractorSet
                                                                             orderby c.Name
                                                                             select c
                                                                              ).ToList());
            }
            catch (Exception ex)
            {
                dbContext.Dispose();
                throw new NotImplementedException(ex.Message, ex); //TODO
            }
        }

        private ICommand _addContractorCommand;
        public ICommand AddContractorCommand
        {
            get
            {
                if (_addContractorCommand == null)
                {
                    _addContractorCommand = new DelegateCommand();
                    ((DelegateCommand)_addContractorCommand).InitializeCommand(val => AddEditNewContractor(null,false));
                }
                return _addContractorCommand;
            }
        }

        private void AddEditNewContractor(object contractor = null,bool editMode = false)
        {
            string title = editMode && contractor != null ? "Edit New Contractor" : "Create New Contractor";
            Window window = new Window
            {
                Title = title,
                Content = new NewContractorDialog(contractor,editMode)
            };
            window.ShowDialog();
            RefreshContractors();
        }

        private ICommand _editContractorCommand;
        public ICommand EditContractorCommand
        {
            get
            {
                if (_editContractorCommand == null)
                {
                    _editContractorCommand = new DelegateCommand();
                    ((DelegateCommand)_editContractorCommand).InitializeCommand(val => AddEditNewContractor(val,true));
                }
                return _editContractorCommand;
            }
        }


        private ICommand _addInvoiceCommand;
        public ICommand AddInvoiceCommand
        {
            get
            {
                if (_addInvoiceCommand == null)
                {
                    _addInvoiceCommand = new DelegateCommand();
                    ((DelegateCommand)_addInvoiceCommand).InitializeCommand(val => AddNewInvoice(val));
                }
                return _addInvoiceCommand;
            }
        }
        private void AddNewInvoice(object contractor)
        {
            Window window = new Window
            {
                Title = "Create New Contractor",
                Content = new NewInvoiceDialog(contractor)
            };
            window.ShowDialog();
            RefreshInvoices();
        }

        private void RefreshInvoices()
        {
            if (SelectedContractor != null)
            {
                try
                {
                    ContractorInvoiceCollection = new ObservableCollection<Invoice>((from i in dbContext.InvoiceSet
                                                                                     where i.Contractor.Id == SelectedContractor.Id
                                                                                     select i).ToList());
                }
                catch (Exception ex)
                {
                    dbContext.Dispose();
                    throw new NotImplementedException(ex.Message, ex); //TODO
                }
            }

        }

        private ICommand _deleteContractorCommand;
        public ICommand DeleteContractorCommand
        {
            get
            {
                if (_deleteContractorCommand == null)
                {
                    _deleteContractorCommand = new DelegateCommand();
                    ((DelegateCommand)_deleteContractorCommand).InitializeCommand(val => DeleteContractor(val));
                }
                return _deleteContractorCommand;
            }
        }

        private void DeleteContractor(object _contractor)
        {
            Contractor contractor = _contractor as Contractor;
            if (contractor != null)
            {
                try
                {
                    var currContractor = (from c in dbContext.ContractorSet
                                          where c.Id == contractor.Id
                                          select c).FirstOrDefault();

                    var currContractorInvoices = (from i in dbContext.InvoiceSet
                                                  where i.Contractor.Id == currContractor.Id
                                                  select i);
                    dbContext.InvoiceSet.RemoveRange(currContractorInvoices);
                    dbContext.ContractorSet.Remove(currContractor);
                    ContractorCollection.Remove(currContractor);

                }
                catch (Exception ex)
                {
                    dbContext.Dispose();
                    throw new NotImplementedException(ex.Message, ex); //TODO
                }
                SaveChangesInDB();
                RefreshContractors();

            }
        }

        private ICommand _deleteInvocieCommand;
        public ICommand DeleteInvocieCommand
        {
            get
            {
                if (_deleteInvocieCommand == null)
                {
                    _deleteInvocieCommand = new DelegateCommand();
                    ((DelegateCommand)_deleteInvocieCommand).InitializeCommand(val => DeleteInvoice(val));
                }
                return _deleteInvocieCommand;
            }
        }

        private void DeleteInvoice(object _invoice)
        {
            Invoice invoice = _invoice as Invoice;
            if (invoice != null)
            {
                try
                {
                    var currInvoice = (from i in dbContext.InvoiceSet
                                       where i.Id == invoice.Id
                                       select i).FirstOrDefault();
                    dbContext.InvoiceSet.Remove(currInvoice);
                    ContractorInvoiceCollection.Remove(currInvoice);
                }
                catch (Exception ex)
                {
                    dbContext.Dispose();
                    throw new NotImplementedException(ex.Message, ex); //TODO
                }

            }
            SaveChangesInDB();
        #endregion

        }

        private ICommand _saveChangesCommand;
        public ICommand SaveChangesCommand
        {
            get
            {
                if (_saveChangesCommand == null)
                {
                    _saveChangesCommand = new DelegateCommand();
                    ((DelegateCommand)_saveChangesCommand).InitializeCommand(val => base.SaveChangesInDB());
                }
                return _saveChangesCommand;
            }
        }
    }
}
