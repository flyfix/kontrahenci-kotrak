﻿using Kontrahenci.Common;
using Kontrahenci.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kontrahenci.ViewModels
{
    public class ViewModelBase : MyNotifyPropertyChanged
    {
        public ViewModelBase()
        {
            try
            {
                dbContext = new DatabaseModelContainer();
            }
            catch (Exception ex)
            {
                
                dbContext.Dispose();
                throw new NotImplementedException(ex.Message,ex); //TODO
                
            }
        }

        protected DatabaseModelContainer dbContext = new DatabaseModelContainer();

        protected void SaveChangesInDB()
        {
            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new NotImplementedException(ex.Message, ex); //TODO
            }
        }
        ~ViewModelBase()
        {
            dbContext.Dispose();
        }

       
    }
}
