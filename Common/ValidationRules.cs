﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Kontrahenci.Common
{
    public class EmailValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            string emailText = value as String;
            try
            {
                MailAddress email = new MailAddress(emailText);
                return new ValidationResult(true, null);
            }
            catch
            {
                return new ValidationResult(false, "Please enter a valid email value.");
            }

           
        }
    }

    public class TelephonValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            string numberText = value as String;
            Regex rg = new Regex(@"^([0-9]{9})|(([0-9]{3}-){2}[0-9]{3})$");
            if (rg.Matches(numberText).Count == 0)
            {
                return new ValidationResult(false, "Please enter a valid number value (123-456-768).");
            }
            else
            {
                return new ValidationResult(true, null);
            }


        }
    }
}
