
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 06/20/2015 18:05:39
-- Generated from EDMX file: c:\users\kstawski\documents\visual studio 2013\Projects\Kontrahenci\DB\DatabaseModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ContractorsDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ContractorSet'
CREATE TABLE [dbo].[ContractorSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Street] nvarchar(max)  NULL,
    [City] nvarchar(max)  NOT NULL,
    [Phone] nvarchar(max)  NULL,
    [Email] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'InvoiceSet'
CREATE TABLE [dbo].[InvoiceSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Amount] float  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Contractor_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'ContractorSet'
ALTER TABLE [dbo].[ContractorSet]
ADD CONSTRAINT [PK_ContractorSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'InvoiceSet'
ALTER TABLE [dbo].[InvoiceSet]
ADD CONSTRAINT [PK_InvoiceSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Contractor_Id] in table 'InvoiceSet'
ALTER TABLE [dbo].[InvoiceSet]
ADD CONSTRAINT [FK_ContractorInvoice]
    FOREIGN KEY ([Contractor_Id])
    REFERENCES [dbo].[ContractorSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ContractorInvoice'
CREATE INDEX [IX_FK_ContractorInvoice]
ON [dbo].[InvoiceSet]
    ([Contractor_Id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------